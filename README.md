# Beginner's Guide to Designing with OpenSCAD

by [Kevin Cole](https://wiki.hacdc.org/index.php?title=User:Ubuntourist)

Released under the [GPL v.3](LICENSE.md).

This is an introduction to designing 3D objects for printing and is
intended for complete newbies.
