// Geodesic.scad
// Designed by Kevin Cole <ubuntourist@hacdc.org> 2019.09.27 (kjc)
//
// Based on the geodesic tree ornament
// See also geodesic.svg
//

polygon([[cos(72 * 1) * 10, sin(72 * 1) * 10],
         [cos(72 * 2) * 10, sin(72 * 2) * 10],
         [cos(72 * 3) * 10, sin(72 * 3) * 10],
         [cos(72 * 4) * 10, sin(72 * 4) * 10],
         [cos(72 * 5) * 10, sin(72 * 5) * 10]]);

//polygon([[cos(72 * 1) * 10, sin(72 * 1) * 10],
//         [cos(72 * 2) * 10, sin(72 * 2) * 10],
