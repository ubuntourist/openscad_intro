#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by Kevin Cole <ubuntourist@hacdc.org> 2019.09.27 (kjc)
#
# Find the length of the sides of the equalateral triangle inscribed in the
# two-inch circle from geodesic.svg.
#
# See also geodesic.svg and geodesic.scad

from math import *

i2mm = 25.400000000000002  # inches to mm

r  = 2.0 * i2mm
x1 = cos(radians(210)) * r
y1 = sin(radians(210)) * r
x2 = cos(radians(90))  * r
y2 = sin(radians(90))  * r
h  = sqrt( ((y2 - y1) ** 2) + ((x2 - x1) ** 2) )
print("   r           = {0} mm".format(r))
print("   r           = {0} in".format(r / i2mm))
#print("   x1          = {0}".format(x1))
#print("   y1          = {0}".format(y1))
#print("   x2          = {0}".format(x2))
#print("   y2          = {0}".format(y2))
#print(" x2 - x1       = {0}".format(x2 - x1))
#print(" y2 - y1       = {0}".format(y2 - y1))
#print("(x2 - x1) ** 2 = {0}".format((x2 - x1) ** 2))
#print("(y2 - y1) ** 2 = {0}".format((y2 - y1) ** 2))
print("   h           = {0} mm".format(h))
print("   h           = {0} in".format(h / i2mm))
