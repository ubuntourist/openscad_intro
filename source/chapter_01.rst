================================
Introduction to the introduction
================================

Working backwards
=================

Mechanical parts
----------------

The common, affordable-ish 3D printers we have at HacDC all work by
feeding a spool of plastic or rubber "thread" a.k.a. *filament*
through an *extruder* and then through a *hot-end* that melts the
filament and "paints" it, layer by layer, onto a *bed* which is often,
but not always heated. The melted filament cools and becomes
semi-solid rather quickly -- solid enough that the next layer of
filament builds on the previous layer, but still soft enough that the
upper layer "bonds" to the layer below it.

In order to achieve this, the printer needs to be able to control (and
monitor) the temperature of the hot-end, the temperature of the bed,
the motor that feeds the filament a.k.a. the *extruder* and three motors
that control the length, width, and height (X, Y, Z) of the extruder.
(Think of the two knobs on an Etch-a-Sketch, if you're old enough. Now,
imagine a third knob for the height.)

Different models of printers implement these functions differently,
since the surface area of the bed, and the height of the printer
varies, as do additional features, such as an LED display panel, a
hot-end cleaner, etc.

Arduino microprocessor
----------------------

The motors, heaters, sensors and other components are all controlled
by a small computer -- a micro-processor -- built into the
printer. Our printers all use a **R**\ epRap **A**\ rduino-compatible
**M**\ other **Bo**\ ard (`RAMBo <https://reprap.org/wiki/Rambo>`_).
Arduino micro-processors are very common and used in quite a variety
of equipment.

Marlin firmware
---------------

Unlike computers used directly by humans, The "operating system" is
not kept on a hard disk or USB stick, but is "burned" into the memory
of the micro-processor. Since it's not as "soft" as software, but not
as "hard" as hardware, it is referred to as *firmware*. The firmware
used by our printers is `Marlin <http://marlinfw.org/>`_.

G-Code
------

This firmware interprets a computer language known as `G-code
<https://en.wikipedia.org/wiki/G-code>`_. G-code instructions consist
of a letter followed by a 1- to 3-digit number followed optionally by
parameters. So, for example,

G1 X140.755 Y129.329 Z0.225

tells the printer to move the hot-end, in a straight line to
X=140.755mm (left or right), Y=129.329 (away or towards the front of
the printer) and Z=0.225 (up or down). The direction will depend on
the position of the hot-end before the instruction is interpreted.

You don't really need to know any G-code, but it is helpful to
know what it looks like, so that you can recognize the contents of
a file as "Oh, that's G-code".

And, know that the final step in printing is to get the G-code into
the printer. Generating the G-code for the specific brand and model
of printer will be the topic of the rest of this document.
